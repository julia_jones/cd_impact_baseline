library(dplyr)

read.csv(file='working/wave_all_1.csv') %>%
  filter(prac != 'pracx') %>%
  group_by(item) %>%
  summarise(sumoft=sum(t), sumofdenom=sum(denom), pct=(sum(t)/sum(denom)*100)) %>%
  write.csv(file='working/raw_baseline_agg_no_pracx.csv', row.names=F)
#  print(n=Inf,width=Inf)

