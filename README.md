# CD IMPACT 
### Chronic Disease early detection and Improved Management in PrimAry Care ProjecT
#### Chronic kidney disease, type 2 diabetes and cardiovascular disease in Australian general practice assessed using electronic medical record data: Cross-sectional baseline data from the CD IMPACT study

This set of scripts associated with the above journal article fixes data issues that need to be addressed prior to data analysis, defines the variables used in subsequent analysis, prepares the data for analysis and provides raw summary statistics as well as  partial pooling analysis.

In this study, data was extracted from general practices in Victoria, Australia using a Pen CS data extraction tool. Further details about Pen CS are avaialble through this [link](https://www.pencs.com.au).

The journal article can be accessed with the following [link](https://fmch.bmj.com/content/10/1/e001006.info?int_source=trendmd&int_medium=cpc&int_campaign=usage-042019).

The following files have been provided:

* cbackstrapolate.R (addresses issues with the data required for analysis)
* cderivedvariables.R (defines the variables used in the study)
* cprepanalysis_all.R (prepares the data for analysis)
* craw_baseline_agg_no_pracx.R (code to run for raw summary statistics of baseline data (excluding a practice which had compromised data quality due to a recent practice merger))
* cpoolbase.R (code to run for partial pooling analysis of baseline data (excluding a practice which had compromised data quality due to a recent practice merger)
