library(dplyr)
library(ggplot2)
library(rstanarm)
library(reshape2)
options(mc.cores = parallel::detectCores())
nchains=4
nsamples=10000
#library(binom)

# read file
db <- read.csv(paste0('working/wave_all_', extractnum, '.csv')) %>%
    rename(pos='t') 

# drop pracx
db <- filter(db, prac != 'pracx') %>%
    filter(prac != excludingthisprac) %>%
    mutate(prac2=paste0('prac',as.numeric(prac)))

# this helps graphs later on
vnames <- unique(db$prac2)
vnames <- vnames[order(vnames)]
db <- merge(db, data.frame(prac2=vnames, prac3=1:length(vnames)), by='prac2')

# helper
expit <- function(x) 1/(1 + exp(-x))

# for predictive distribution later on
newdb <- data.frame(prac2='new practice', denom=1, pos=-1)

# partial pooling analysis
db.out <- data.frame()
for(itemised in unique(db$item)){
    dbsub <- subset(db, item==itemised)
    model <- stan_glmer(cbind(pos,denom-pos) ~  (1|prac2), 
        data=dbsub, 
        family=binomial, 
        chains=nchains, 
        iter=2*nsamples,
        adapt_delta=.99)
    temp <- model %>% 
        as_data_frame
    names(temp) <- c('interc', vnames, 'sigma')
    # get partial pooled practice-specific estimates
    dbplot <- data.frame()
    for(j in seq(along=vnames)){
        postj  <- expit(unlist(temp[,vnames[j]] + temp[,1]))
        dbplot <- rbind(dbplot, data.frame(variable=vnames[j], x=j, ll=quantile(postj, probs=0.025), med=quantile(postj, probs=0.5), ul=quantile(postj, probs=0.975)))
        }
    # partial pooled grand mean
    postj  <- expit(unlist(temp[,1]))
    partialpool <- quantile(postj, probs=0.5)
    # total pool grand mean
#    totalpool <- binom.confint(sum(dbsub$pos),sum(dbsub$denom), methods='bayes')

    # partial pooled "new" practice
    postj <- expit(posterior_linpred(model, newdata=newdb))
    partialpool.newll <- quantile(postj, 0.025)
    partialpool.newul <- quantile(postj, 0.975)

    print(
    ggplot(dbplot) + 
        geom_point(aes(x=x+0.05, y=med), colour='blue') +
        geom_errorbar(aes(x=x+0.05, ymin=ll, ymax=ul), colour='blue', width=0.05) +
        geom_point(aes(x=prac3-0.05, y=y/100), colour='orange', data=dbsub) +
        geom_errorbar(aes(x=prac3-0.05, ymin=ll, ymax=ul), colour='orange', data=dbsub, width=0.05) +
        geom_hline(aes(yintercept=partialpool)) + 
        geom_hline(aes(yintercept=partialpool.newll), linetype=2, colour='grey50') + 
        geom_hline(aes(yintercept=partialpool.newul), linetype=2, colour='grey50') + 
        theme_bw() +
        theme(legend.position='none') +
        scale_y_continuous('incidence', limits=0:1) +
        scale_x_continuous('practice', breaks=seq(along=vnames))
    )

    print(
    ggplot(dbplot) + 
        geom_point(aes(x=x+0.05, y=med), colour='blue') +
        geom_errorbar(aes(x=x+0.05, ymin=ll, ymax=ul), colour='blue', width=0.05) +
        geom_point(aes(x=prac3-0.05, y=y/100), colour='orange', data=dbsub) +
        geom_errorbar(aes(x=prac3-0.05, ymin=ll, ymax=ul), colour='orange', data=dbsub, width=0.05) +
        geom_hline(aes(yintercept=partialpool)) + 
        geom_hline(aes(yintercept=partialpool.newll), linetype=2, colour='grey50') + 
        geom_hline(aes(yintercept=partialpool.newul), linetype=2, colour='grey50') + 
        theme_bw() +
        theme(legend.position='none') +
        scale_y_continuous('incidence') +
        scale_x_continuous('practice', breaks=seq(along=vnames))
    )

    cat(itemised)
    cat('\n"average practice ')
    cat(round(100*partialpool,1))
    cat('   [')
    cat(round(100*partialpool.newll,1))
    cat(' ; ')
    cat(round(100*partialpool.newul,1))
    cat(']\n')

    db.out <- rbind(db.out, data.frame(item=itemised, med=round(partialpool,4), ll=round(partialpool.newll,4), ul=round(partialpool.newul,4)))
    }

row.names(db.out) <- NULL

